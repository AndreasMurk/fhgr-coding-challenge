package com;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Main {

    // Design Task

    // create a backend which can support all CRUD operations (Create, Read Update and Delete)
    // this backend supports all RESTful services - create endpoints for reading to provide backend solution.
    // create a database connection to it to persist all the sent data (jdbc with Java)
    // use object relational mapping when working with an object-oriented programming language and create necessary classes for it

    // whenever a HTTP-Request is coming (GET 200) -> return a JSON representation of persisted data class (create methods for serializing)
    // create a service that can retrieve different APIs and saves the JSON response and persist it in database.
    // whenever a new high API is added, simply add it to the list which is iterated through to retrieve all data and make it scalable
    // to search data via an endpoint, also provide different endpoints with 1. single or 2. multiple search query parameters
    // focus on SOLID principles - foremost the open closed principle (open for extension, closed for modiciation)
    // only produce code to extend the software architecture if needed

    public static void main(String[] args) throws IOException {

        File file1 = new File("/home/andreas-notebook/Developing/FHG/coding-challenge-FHG-java/src/com/2010100102.txt");
        File file2 = new File("/home/andreas-notebook/Developing/FHG/coding-challenge-FHG-java/src/com/2010112901.txt");
        BufferedReader br = new BufferedReader(new FileReader(file1));
        BufferedReader br2 = new BufferedReader(new FileReader(file2));

        String[] words1 = null;
        String[] words2 = null;

        ArrayList<String> bothDocumentWordList = new ArrayList<>();

        Map<Integer, String> frequencyWords1 = new HashMap<>();
        Map<Integer, String> frequencyWords2 = new HashMap<>();

        words1 = getStrings(br, words1);

        words2 = getStrings(br2, words2);

        // compute the occurences of words in document1
        computeFrequencies(words1, words2, frequencyWords1, frequencyWords2);

        // compute the occurences of words in document2
        int totalFrequency = computeTotalFrequency(words1, words2, bothDocumentWordList);

        // write all output to a specified CSV file
        writeToCSV(bothDocumentWordList, frequencyWords1.values(), frequencyWords2.values(), totalFrequency, "output.csv");

    }

    private static String[] getStrings(BufferedReader br, String[] words1) throws IOException {
        String outputString1;
        // loop through all lines and split with given delimiter and save it to given array
        while ((outputString1 = br.readLine()) != null) {
            words1 = outputString1.split(" ");
        }
        return words1;
    }

    private static int computeTotalFrequency(String[] words1, String[] words2, ArrayList<String> bothDocumentWordList) {
        int totalFrequency = 0;

        // look for both words arrays and check if same occurence of word is found
        // if found, increase totalFrequency and return it
        for (String s : words1) {
            for (String s2 :
                    words2) {
                if (s == s2) {
                    totalFrequency++;
                    // add appearing word to list
                    bothDocumentWordList.add(s.toLowerCase());
                }
            }
        }
        return totalFrequency;
    }

    private static void computeFrequencies(String[] words1, String[] words2, Map<Integer, String> frequencyWords1, Map<Integer, String> frequencyWords2) {
        for (String s :
                words1) {
            // check if word is already in Map
            if (frequencyWords1.containsKey(s)) {
                // increase frequency
                frequencyWords1.get(s) += 1;
            }
        }

        for (String s :
                words2) {
            // check if word is already in Map
            if (frequencyWords2.containsKey(s)) {
                // increase frequency
                frequencyWords1.get(s) += 1;
            }
        }
    }

    void writeToCSV(ArrayList<String> words, int frequency1, int frequency2, int totalFrequency, String fileName) throws IOException {
        File csvFile = new File(fileName);
        FileWriter fileWriter = new FileWriter(csvFile);

        int counterFreq1 = 0;
        int counterFreq2 = 0;

        for (String word :
                words) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(word + "," + frequency1[counterFreq1] + "," + frequency2[counterFreq2] + ", " + totalFrequency);
            fileWriter.write(stringBuilder.toString());
            counterFreq1++;
            counterFreq2++;
        }
        fileWriter.close();
    }
}
